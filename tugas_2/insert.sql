CREATE DATABASE IF NOT EXISTS sdm;

USE sdm;

CREATE TABLE IF NOT EXISTS departemen (
    id INT NOT NULL AUTO_INCREMENT,
    nama VARCHAR(20),
    PRIMARY KEY (id));

CREATE TABLE IF NOT EXISTS karyawan (
    id INT NOT NULL AUTO_INCREMENT,
    nama VARCHAR(50),
    jenis_kelamin ENUM('L', 'P'),
    status ENUM('Belum', 'Menikah'),
    tanggal_lahir DATE,
    tanggal_masuk DATE,
    departemen INT,
    PRIMARY KEY (id),
    FOREIGN KEY (departemen) REFERENCES departemen(id)
    );